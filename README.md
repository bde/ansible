# Recettes Ansible du BDE

Ensemble des recettes de déploiement Ansible pour les serveurs du BDE.
Pour les utiliser, vérifiez que vous avez au moins Ansible 2.7.

## Ansible 101

Si vous n'avez jamais touché à Ansible avant, voilà une rapide introduction.

**Inventory** : c'est le fichier `hosts` d'inventaire.
Il contient la définition de chaque machine et le regroupement.

Quand on regroupe avec un `:children` en réalité on groupe des groupes.

Chaque machine est annoncée avec son hostname. Il faut pouvoir SSH sur cette machine
avec ce hostname, car c'est ce qu'Ansible fera.

**Playbook** : c'est une politique de déploiement.
Il contient les associations des rôles avec les machines.

L'idée au Crans est de regrouper par thème. Exemple, le playbook `monitoring.yml`
va contenir toutes les définitions machines-rôles qui touchent au monitoring.
Cela permet de déployer manuellement tout le monitoring sans toucher au reste.

**Rôle** : un playbook donne des rôles à des machines. Ces rôles sont tous dans
le dossier `roles/`. Un rôle installe un service précis sur un serveur.

Il est préférable d'être atomique sur les rôles plutôt d'en coder un énorme
qui sera difficilement maintenable.

*Exemples de rôle* : activer les backports pour ma version de Debian, installer NodeJS,
déployer un serveur prometheus, déployer une node prometheus…

**Tâche** : un rôle est composé de tâches. Une tâche effectue une et une seule
action. Elle est associée à un module Ansible.

*Exemples de tâche* : installer un paquet avec le module `apt`, ajouter une ligne dans
un fichier avec le module `lineinfile`, copier une template avec le module `template`…

Une tâche peut avoir des paramètres supplémentaires pour la réessayer quand elle plante,
récupérer son résultat dans une varible, mettre une boucle dessus, mettre des conditions…

N'oubliez pas d'aller lire l'excellent documentation de RedHat sur tous les modules
d'Ansible !

### Lister tout ce que sait Ansible sur un hôte

Lors du lancement d'Ansible, il collecte un ensemble de faits sur les serveurs
qui peuvent ensuite être utilisés dans des variables.
Pour lister tous les faits qu'Ansible collecte nativement d'un serveur
on peut exécuter le module `setup` manuellement.

```
ansible note.crans.org -m setup
```

## Exécution d'Ansible

### Lancer Ansible

Il faut sa clé SSH configurée sur le serveur que l'on déploit.
```bash
ssh-copy-id note.crans.org
```

Pour tester le playbook `base.yml` :
```bash
./base.yml --check
```

Vous pouvez ensuite enlever `--check` si vous voulez appliquer les changements !

Vous pouvez aussi avoir un mode étape par étape avec `--step`.

Si vous avez des soucis de fingerprint ECDSA, vous pouvez ignorer une
première fois (dangereux !) : `ANSIBLE_HOST_KEY_CHECKING=0 ansible-playbook...`.

# Installation du server LDAP

Sur l'ancien serveur, faire une backup :

```
slapcat -n 0 > schema.ldif
slapcat -n 1 > bdd.ldif
```

Il faut un certificat au nom du nouveau serveur, voir <https://wiki.crans.org/NoteKfet/NoteKfet2015/LDAPS>.

Sur le nouveau server, importer la backup :

```
systemctl stop slapd
systemctl stop nslcd
rm -rf /etc/ldap/slapd.d/* /var/lib/ldap/*
slapadd -n 0 -l schema.ldif -F /etc/ldap/slapd.d/
slapadd -n 1 -l bdd.ldif
chown openldap:openldap -R /var/lib/ldap/ /etc/ldap/slapd.d/
chown root:openldap /etc/ldap/ssl/ldap-bde-key.pem /etc/ldap/ssl/ldap-bde-cert.pem
chmod 640 /etc/ldap/ssl/ldap-bde-key.pem
chmod 644 /etc/ldap/ssl/ldap-bde-cert.pem
nscd -i group
nscd -i passwd
systemctl start slapd
systemctl start nslcd
id erdnaxe  # pour tester
```
